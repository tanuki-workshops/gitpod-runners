#!/bin/bash

GITLAB_URL="https://gitlab.com/"
GITLAB_API="https://gitlab.com/api/v4"
GROUP_ID="5085244"
CICD_RUNNER_TOKEN="${TANUKI_WORKSHOP_RUNNERS_TOKEN}" # look at the CI/CD variables


# Gitpod full
docker run --rm -it -v gitlab-runner01-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image gitpod/workspace-full \
  --description "🍊 Gitpod runner (full)" \
  --tag-list "gitpod-full" \
  --run-untagged="false" \
  --locked="false"

# Gitpod full
docker run --rm -it -v gitlab-runner02-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image gitpod/workspace-full \
  --description "🍊 Gitpod runner (full)" \
  --tag-list "gitpod-full" \
  --run-untagged="false" \
  --locked="false"

# Alpine
docker run --rm -it -v gitlab-runner03-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "🟦 Alpine" \
  --tag-list "alpine-simple" \
  --run-untagged="false" \
  --locked="false"

# Node.js
docker run --rm -it -v gitlab-runner04-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image node:alpine \
  --description "🟩 Node.js runner" \
  --tag-list "alpine-node-js" \
  --run-untagged="false" \
  --locked="false"

# Gitpod base
docker run --rm -it -v gitlab-runner05-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image gitpod/workspace-base \
  --description "🍊 Gitpod runner (base)" \
  --tag-list "gitpod-base" \
  --run-untagged="false" \
  --locked="false"

# Gitpod base
docker run --rm -it -v gitlab-runner06-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "${GITLAB_URL}" \
  --registration-token "${CICD_RUNNER_TOKEN}" \
  --executor "docker" \
  --docker-image gitpod/workspace-base \
  --description "🍊 Gitpod runner (base)" \
  --tag-list "gitpod-base" \
  --run-untagged="false" \
  --locked="false"


